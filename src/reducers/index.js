import { combineReducers } from "redux";

export const hitParams = (
  state = {
    page: 1,
    description: "",
    location: "",
    full_time: false,
  },
  action
) => {
  switch (action.type) {
    case "SET_PARAMS":
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export const listReducer = (
  state = {
    loading: false,
    data: [],
    endPage: false,
    dataDetail: {},
    loadingDetail: false,
  },
  action
) => {
  switch (action.type) {
    case "SET_PENDING":
      return {
        ...state,
        loading: action.loading,
      };
    case "SET_DATA_JOB":
      return {
        ...state,
        data: action.payload,
        loading: false,
      };
    case "SET_END_PAGE":
      return {
        ...state,
        endPage: action.end,
      };
    case "SET_PENDING_DETAIL":
      return {
        ...state,
        loadingDetail: action.loading,
      };
    case "SET_DATA_JOB_DETAIL":
      return {
        ...state,
        dataDetail: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  hitParams: hitParams,
  listReducer: listReducer,
});

export default rootReducer;
