import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card, Container } from "react-bootstrap";
import InfiniteScroll from "react-infinite-scroll-component";
import Spinner from "react-bootstrap/Spinner";
import Moment from "react-moment";
import { useNavigate } from "react-router-dom";

const List = (p) => {
  const { list, fetchMoreData, setIsDetail } = p;
  const navigate = useNavigate();

  const filteredArray = Object.values(
    list.data.reduce((unique, o) => {
      if (!unique[o.title] || +o.id > +unique[o.title].id) unique[o.title] = o;

      return unique;
    }, {})
  );

  const goToDetail = (id) => {
    navigate(`?detail=${id}`);
    setIsDetail(true);
  };

  return (
    <Container className="mt-3">
      {filteredArray.length ? <h3 className="mb-3">Job List</h3> : ""}

      <InfiniteScroll
        dataLength={filteredArray.length}
        next={fetchMoreData}
        hasMore={!list.endPage}
        loader={
          !list.endPage ? (
            <Spinner
              className="text-center mt-5 mb-5"
              animation="border"
              role="status"
            >
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          ) : (
            ""
          )
        }
        endMessage={
          <p className="text-center mt-5 mb-5">
            <b>
              No more data <i>!!</i>
            </b>
          </p>
        }
      >
        {filteredArray.map((data, index) => (
          <Card
            onClick={() => goToDetail(data.id)}
            className="mb-3 pointer"
            key={index}
            style={{ width: "100%" }}
          >
            <Card.Body className="d-flex justify-content-between">
              <div>
                <Card.Title>{data.title}</Card.Title>
                <Card.Text>
                  {data.company}
                  <span> - {data.type} </span>
                </Card.Text>
              </div>
              <div className="text-end">
                <Card.Text className="mb-1">{data.location}</Card.Text>
                <Card.Text>
                  <span>
                    <Moment fromNow>{data.created_at}</Moment>{" "}
                  </span>
                </Card.Text>
              </div>
            </Card.Body>
          </Card>
        ))}
      </InfiniteScroll>
    </Container>
  );
};

export default List;
