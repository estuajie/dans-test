import React, { useState, useEffect } from "react";
import { Container, Button, Row, Col, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";

const SearchForm = (p) => {
  const { params, setParams, resetData } = p;
  const [job, setJob] = useState("");
  const [location, setLocation] = useState("");
  const [fullTime, setFullTime] = useState(false);

  const dispatch = useDispatch();

  const searchAction = () => {
    dispatch(resetData());
    dispatch(
      setParams({
        ...params,
        page: 1,
        description: job,
        location: location,
        full_time: fullTime,
      })
    );
  };

  return (
    <Container>
      <Row className="mt-4 d-flex justify-content-between">
        <Col xs={4}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Job Description</Form.Label>
            <Form.Control
              type="job"
              placeholder="Description"
              onChange={(e) => setJob(e.target.value)}
            />
          </Form.Group>
        </Col>
        <Col xs={4}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Location</Form.Label>
            <Form.Control
              type="location"
              placeholder="City name"
              onChange={(e) => setLocation(e.target.value)}
            />
          </Form.Group>
        </Col>
        <Col xs={2}>
          <Form.Check
            className="mt-custom"
            type="checkbox"
            id="custom-checkbox"
            label="Full Time Only"
            onClick={() => setFullTime(!fullTime)}
          />
        </Col>
        <Col xs={2}>
          <Button className="mt-custom-button" onClick={() => searchAction()}>
            Search Your Job
          </Button>
        </Col>
      </Row>
    </Container>
  );
};

export default SearchForm;
