import React, { useEffect } from "react";
import { useGoogleLogin } from "@react-oauth/google";
import axios from "axios";
import { Button } from "react-bootstrap";

const Welcome = (p) => {
  const { cookie, isLogin, setIsLogin, setProfile, setCookie, profile } = p;

  const login = useGoogleLogin({
    onSuccess: (codeResponse) =>
      setCookie("access_token", codeResponse.access_token),
    onError: (error) => console.log("Login Failed:", error),
  });

  useEffect(() => {
    if (cookie.access_token !== undefined) {
      axios
        .get(
          `https://www.googleapis.com/oauth2/v1/userinfo?access_token=${cookie.access_token}`,
          {
            headers: {
              Authorization: `Bearer ${cookie.access_token}`,
              Accept: "application/json",
            },
          }
        )
        .then((res) => {
          setProfile(res.data);
          setIsLogin(true);
        })
        .catch((err) => console.log(err));
    }
  }, [cookie]);

  return (
    <>
      {!isLogin ? (
        <div className="position-absolute top-50 start-50 translate-middle">
          <h1 className="mb-0 font-bold">welcome</h1>
          <p className="mb-4">
            Please login with your google account for continue
          </p>
          <Button onClick={() => login()}>Sign in with Google </Button>
        </div>
      ) : (
        ""
      )}
    </>
  );
};

export default Welcome;
