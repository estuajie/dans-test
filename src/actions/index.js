import Axios from "axios";

// Helper
export const encodeQueryData = (data) => {
  let ret = [];

  for (let d in data) {
    let value = data[d];

    if (typeof value == "object") {
      value = data[d].join(",");
    }

    ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(value));
  }

  return ret.join("&");
};

export const setParams = (params) => {
  return (dispatch) => {
    dispatch({ type: "SET_PARAMS", payload: params });
  };
};

export const resetData = () => {
  return (dispatch) => {
    dispatch({ type: "SET_DATA_JOB", payload: [] });
  };
};

export const resetDataDetail = () => {
  return (dispatch) => {
    dispatch({ type: "SET_DATA_JOB_DETAIL", payload: {} });
  };
};

export const getDataJobs = (list, params) => {
  return (dispatch) => {
    dispatch({ type: "SET_PENDING", loading: true });
    dispatch({ type: "SET_END_PAGE", end: false });
    return Axios.get(
      `http://dev3.dansmultipro.co.id/api/recruitment/positions.json${
        Object.keys(params).length > 0 ? "?" + encodeQueryData(params) : ""
      }`
    )
      .then((res) => {
        let response = res.data.filter((x) => x !== null);
        if (response.length) {
          dispatch({
            type: "SET_DATA_JOB",
            payload: list.concat(response),
          });
          dispatch({ type: "SET_PENDING", loading: false });
        } else {
          dispatch({ type: "SET_END_PAGE", end: true });
        }
      })
      .catch((err) => {
        dispatch({ type: "SET_PENDING", loading: false });
        dispatch({ type: "SET_END_PAGE", end: true });
        throw err;
      });
  };
};

export const getDataJobsDetail = (id) => {
  return (dispatch) => {
    dispatch({ type: "SET_PENDING", loading: true });
    return Axios.get(
      `http://dev3.dansmultipro.co.id/api/recruitment/positions/${id}`
    )
      .then((res) => {
        dispatch({
          type: "SET_DATA_JOB_DETAIL",
          payload: res.data,
        });
        dispatch({ type: "SET_PENDING_DETAIL", loading: false });
      })
      .catch((err) => {
        dispatch({ type: "SET_PENDING_DETAIL", loading: false });
        throw err;
      });
  };
};
