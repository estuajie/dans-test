import React, { useState, useEffect } from "react";
import { Container, Button } from "react-bootstrap";
import Search from "./Search";
import Detail from "./Detail";

const Main = () => {
  const [isDetail, setIsDetail] = useState(false);

  const props = {
    isDetail,
    setIsDetail,
  };

  return (
    <Container>
      {isDetail ? <Detail {...props} /> : <Search {...props} />}
    </Container>
  );
};

export default Main;
