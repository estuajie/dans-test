import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button, Card } from "react-bootstrap";
import { useSearchParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getDataJobsDetail, resetDataDetail } from "../actions";
import Image from "react-bootstrap/Image";

const Detail = (props) => {
  const { isDetail, setIsDetail } = props;
  const [searchParams, setSearchParams] = useSearchParams();
  const dispatch = useDispatch();
  const parse = require("html-react-parser");

  const list = useSelector((state) => state.listReducer);
  const id = searchParams.get("detail");
  const detail = list.dataDetail;

  const goToBack = () => {
    setIsDetail(false);
    dispatch(resetDataDetail());
    const param = searchParams.get("detail");
    if (param) {
      searchParams.delete("detail");
      setSearchParams(searchParams);
    }
  };

  useEffect(() => {
    dispatch(getDataJobsDetail(id));
  }, []);

  return (
    <Container>
      <Button
        className="mt-4 mb-5"
        variant="outline-primary"
        onClick={() => goToBack()}
      >
        Back
      </Button>
      <Row className="d-flex justify-content-between">
        <Col xs={8}>
          <p className="mb-0">
            {detail?.type} - {detail?.location}
          </p>
          <h4>{detail?.title}</h4>
          <p className="mt-5">{parse(detail?.description || "")}</p>
        </Col>
        <Col xs={4}>
          <p className="mb-0">
            <strong>{detail?.company}</strong>
          </p>
          <p className="mb-0">
            <i>{detail?.company_url}</i>
          </p>
          <Card className="mt-4 mb-0 p-3">
            <h4>How to Apply ?</h4>
            <p className="mb-0">{parse(detail?.how_to_apply || "")}</p>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default Detail;
