import React, { useState, useEffect } from "react";
import { Container, Button } from "react-bootstrap";
import Image from "react-bootstrap/Image";
import Welcome from "../component/Welcome";
import SearchForm from "../component/SearchForm";
import List from "../component/List";
import { useCookies } from "react-cookie";
import { googleLogout } from "@react-oauth/google";
import { useDispatch, useSelector } from "react-redux";
import { getDataJobs, setParams, resetData } from "../actions";

const Search = (props) => {
  const { setIsDetail } = props;
  const [isLogin, setIsLogin] = useState(false);
  const [profile, setProfile] = useState([]);
  const [cookie, setCookie, removeCookie] = useCookies(["access_token"]);

  const params = useSelector((state) => state.hitParams);
  const list = useSelector((state) => state.listReducer);

  const dispatch = useDispatch();

  const logOut = () => {
    googleLogout();
    setProfile(null);
    removeCookie("access_token", { path: "/" });
    setIsLogin(false);
  };

  const fetchMoreData = () => {
    if (!list.endPagel) {
      setTimeout(() => {
        dispatch(
          setParams({
            ...params,
            page: params.page + 1,
          })
        );
      }, 1500);
    }
  };

  useEffect(() => {
    dispatch(getDataJobs(list.data, params));
  }, [params]);

  const p = {
    isLogin,
    setIsLogin,
    profile,
    setProfile,
    cookie,
    setCookie,
    removeCookie,
    logOut,
    fetchMoreData,
    params,
    setParams,
    resetData,
    setIsDetail,
  };

  return (
    <Container>
      {!isLogin ? (
        <Welcome {...p} />
      ) : (
        <>
          <header className="d-flex justify-content-between">
            <h1 className="pt-4">
              <strong>Github</strong> Jobs
            </h1>
            <div className="d-flex justify-content-between">
              <Image
                rounded
                width={80}
                height={80}
                src={profile.picture}
                alt="user"
              />
              <span>
                <p className="m-0">
                  <strong>{profile.name}</strong>
                </p>
                <p className="m-0 profile-email">{profile.email}</p>
                <Button
                  className="mt-1 mb-2"
                  variant="dark"
                  size="sm"
                  onClick={() => logOut()}
                >
                  Logout
                </Button>
              </span>
            </div>
          </header>

          <SearchForm {...p} />
          <List {...p} list={list} params={params} setIsDetail={setIsDetail} />
        </>
      )}
    </Container>
  );
};

export default Search;
